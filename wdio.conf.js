const path = require("path")
const funding = require("./pageObjects/funding")
const mydaad = require("./pageObjects/mydaad")
const myguide = require("./pageObjects/myguide")
const portal = require("./pageObjects/portal")
require("dotenv").config()

exports.config = {
    runner: "local",
    specs: [path.join("tests", "*.js")],
    exclude: [],
    // baseUrl: '', // handeled by the page object
    maxInstances: 1,
    maxInstancesPerCapability: 1,
    capabilities: [
        {
            browserName: "chrome",
            maxInstances: 1,
            "goog:chromeOptions": {
                w3c: false,
                args: ["--window-size=1440,800", "--headless"]
            }
        }
    ],
    wdi5: {
        // path: "", // commented out to use the default paths
        screenshotPath: path.join("./", "report", "screenshots"),
        logLevel: "error", // error | verbose | silent
        platform: "browser", // electron, browser, android, ios
        deviceType: "web"
    },
    services: ["chromedriver"],
    execArgv: [],
    logLevel: "debug",
    logLevels: {
        webdriver: "silent"
    },
    bail: 0,
    waitforTimeout: 90000,
    filesToWatch: [],
    framework: "mocha",
    mochaOpts: {
        timeout: 90000
    },
    outputDir: path.join("./", "report"),
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    reporters: ["spec"],

    before: function (capabilities, specs) {
        // setup
        // inject same browser object due to session into page
        const _mydaad = mydaad(browser)
        const _myguide = myguide(browser)
        const _portal = portal(browser)
        const _funding = funding(browser)

        if (_mydaad && _myguide && _portal && _funding) {
            console.log("🚀 SP initialized successful")
        } else {
            console.error("SP initialization failed")
        }
    },

    after: function () {}
}
