const mydaad = require("../pageObjects/mydaad")

module.exports = {
    testLogin() {
        it("should be logged in on mydaad", () => {
            mydaad().open()
            mydaad().doSSOLogin()
            expect(mydaad().isUserLoggedIn()).toBeTruthy()
        })
    },

    doLogin() {
        it("should login on mydaad", () => {
            mydaad().open()
            mydaad().login()
            expect(mydaad().isUserLoggedIn()).toBeTruthy()
        })
    },

    checkLogout() {
        it("should be logged out on mydaad", () => {
            mydaad().open()
            mydaad().browser.refresh()
            expect(mydaad().isUserNotLoggedIn()).toBeTruthy()
        })
    },

    logout() {
        it("should log-out out on mydaad", () => {
            mydaad().open()
            mydaad().logout()
            mydaad().browser.refresh()
            expect(mydaad().isUserNotLoggedIn()).toBeTruthy()
        })
    }
}
