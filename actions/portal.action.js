const portal = require("../pageObjects/portal")

module.exports = {
    testLogin() {
        it("should be logged in on portal", () => {
            portal().open()

            // trigger login
            // special case and not needed due to direct redirect
            // portal().triggerSSOLogin()

            // expect to be logged in though SSO
            expect(portal().isUserLoggedIn()).toBeTruthy()
        })
    },

    doLogin() {
        it("should login on portal", () => {
            portal().open()

            // trigger login
            portal().login()

            // expect to be logged in though SSO
            expect(portal().isUserLoggedIn()).toBeTruthy()
        })
    },

    checkLogout() {
        it("should be logged out on the portal", () => {
            portal().open()
            portal().browser.refresh()
            expect(portal().isUserNotLoggedIn()).toBeTruthy()
        })
    },

    logout() {
        it("should log-out out on portal", () => {
            portal().open()
            portal().logout()
            portal().browser.refresh()
            expect(portal().isUserNotLoggedIn()).toBeTruthy()
        })
    }
}
