const funding = require("../pageObjects/funding")

module.exports = {
    testLogin() {
        it("should be logged in on funding", () => {
            funding().open()
            expect(funding().isUserLoggedIn()).toBeTruthy()
        })
    },

    doLogin() {
        it("should login on funding", () => {
            funding().open()
            funding().login()
            expect(funding().isUserLoggedIn()).toBeTruthy()
        })
    },

    checkLogout() {
        it("should be logged out on funding", () => {
            funding().open()
            funding().browser.refresh()
            expect(funding().isUserNotLoggedIn()).toBeFalsy()
        })
    },

    logout() {
        it("should log-out out on funding", () => {
            funding().open()
            funding().logout()
            funding().browser.refresh()
            expect(funding().isUserNotLoggedIn()).toBeTruthy()
        })
    }
}
