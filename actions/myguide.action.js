const myguide = require("../pageObjects/myguide")

module.exports = {
    testLogin() {
        it("should be logged in on myguide", () => {
            myguide().open()
            // trigger login
            myguide().triggerSSOLogin()
            // expect to be logged in though SSO
            expect(myguide().isUserLoggedIn()).toBeTruthy()
        })
    },

    doLogin() {
        it("should login in on myguide", () => {
            myguide().open()

            // trigger login
            myguide().login()

            // expect to be logged in though SSO
            expect(myguide().isUserLoggedIn()).toBeTruthy()
        })
    },

    logout() {
        it("should log-out out on myguide", () => {
            myguide().open()
            myguide().logout()
            myguide().browser.refresh()
            expect(myguide().isUserNotLoggedIn()).toBeTruthy()
        })
    },

    checkLogout() {
        it("should be logged out on the myguide", () => {
            myguide().open()
            myguide().browser.refresh()
            expect(myguide().isUserNotLoggedIn()).toBeTruthy()
        })
    }
}
