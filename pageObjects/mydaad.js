const page = require("./page")

let _instance = null

class mydaad extends page {
    _url = "https://mydaad.de"
    _title = "My DAAD"
    _xPath = {
        loginButton: "/html/body/main/article/div[2]/div/a[1]",
        userMenu: "/html/body/header/div[1]/nav/ul[2]/li[2]/div/a/span",
        logoutButton: "/html/body/header/div[1]/nav/ul[2]/li[2]/div/div/ul/li[4]/a"
    }
    _ids = {
        loginButton: "loginButton",
        userMenu: "userMenu",
        logoutButton: "menuLogout"
    }

    constructor(browser) {
        super(browser)
        return this
    }

    login() {
        // login button
        const loginButton = this.browser.$(this._ids.loginButton)
        loginButton.waitForDisplayed()
        if (loginButton.isClickable()) {
            loginButton.click()
        }
        // enter text and press login button
        this._doLogin()

        // check the browser title to verify
        // wait for the pageload
        this.browser.$("/html/body/main").waitForDisplayed()
    }

    doSSOLogin() {
        const loginButton = this.browser.$(this._ids.loginButton)
        if (loginButton.isClickable()) {
            loginButton.click()
        }
    }

    isUserNotLoggedIn() {
        const loginButton = this.browser.$(this._ids.loginButton)
        this.browser.waitUntil(() => loginButton.isExisting())

        this.browser.$("/html/body/main").waitForDisplayed()
        // TODO: don't string compare
        const loginButtonText = loginButton.getText()
        return loginButtonText === "ANMELDEN"
    }

    isUserLoggedIn() {
        // return this.browser.getTitle() === "My DAAD - My DAAD"
        return this.browser.$(this._ids.userMenu).getText() === process.env.USERNAME
    }

    logout() {
        const userMenu = this.browser.$(this._ids.userMenu)
        this.browser.waitUntil(() => userMenu.isExisting())
        userMenu.click()

        const logoutButton = this.browser.$(this._ids.logoutButton)
        this.browser.waitUntil(() => logoutButton.isClickable())
        logoutButton.click()
    }
}

// singleton
module.exports = (browser) => {
    if (!_instance && browser) {
        _instance = new mydaad(browser)
    }
    return _instance
}
