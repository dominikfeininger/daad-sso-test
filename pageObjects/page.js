// abstract
module.exports = class page {
    browser = null
    _tab = ""
    _titleLoginPage = "DAAD-ID"
    _isOpen = false
    // defined in subclass
    _url = ""
    // title of tab
    _title = ""
    _xPathPage = {
        loginForm: "/html/body/div[2]/div/div[1]/div/div/div[1]/form/div[3]"
    }

    constructor(browser) {
        this.browser = browser
    }

    navigate(url) {
        this.browser.navigateTo(url)
    }

    open() {
        if (this._isOpen) {
            // already open -> just focus window
            this.browser.switchToWindow(this._tab)
        } else {
            // create tab
            if (this.browser.getTitle() === "") {
                // first -> navigate to url
                this.browser.url(this._url)
            } else {
                // open more tabs after that
                this.browser.execute((url) => {
                    // browser context - you may not access client or console
                    window.open(url)
                }, this._url)
            }

            const allTabs = this.browser.getWindowHandles()
            // last tab is the new one
            this._tab = allTabs[allTabs.length - 1]

            this.browser.switchToWindow(this._tab)
            this._isOpen = true
        }
    }

    close() {
        this.browser.deleteSession()
    }

    _doLogin() {
        // username input
        const usernameInput = this.browser.$(this._xPathPage.loginForm + "/div[1]/input")
        usernameInput.waitForDisplayed()
        usernameInput.setValue(process.env.USERNAME)

        this.browser.$(this._xPathPage.loginForm + "/div[2]/input").setValue(process.env.PASSWORD)

        // press the button
        this.browser.$(this._xPathPage.loginForm + "/div[4]/button").click()
    }

    getTitle() {
        return this._title
    }
}
