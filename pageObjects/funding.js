const wdi5 = require("wdi5")
const page = require("./page")

let _instance = null

// ui5 page
class funding extends page {
    _url = "https://funding.mydaad.de" //B2C_1A_signup_signin_SCP_saml
    _title = "Personenförderung | MyDAAD"
    // bool to identify if page is on a ui5 site after open
    // sap.ui.version "1.72.1"
    _onUI5Page = false
    _xPath = {
        mainSection: "/html/body/div[2]/div/div/div/div/div[2]/div/section/div/div[2]/div/div/div/div/div/div/section",
        userMenu:
            "/html/body/div[2]/div/div/div/div/div[2]/div/section/div/div[2]/div/div/div/div/div/div/section" +
            "/div[1]/div/div/div[1]/div/nav/ul[2]/li[2]/div"
    }

    constructor(browser) {
        super(browser)
        return this
    }

    open() {
        super.open()

        // check for ui5 page
        // wait till page is loaded
        this._checkForUi5PageAndInit()
    }

    _checkForUi5PageAndInit() {
        this.browser.waitUntil(
            () => {
                const readyState = this.browser.executeAsync((done) => {
                    setTimeout(() => {
                        done(document.readyState)
                    }, 400)
                })
                return readyState === "complete"
            },
            { interval: 500, timeout: 8000 }
        )

        // test for ui5
        const sapExists = this.browser.execute(() => {
            // browser context - you may not access client or console
            return !!window.sap
        })

        if (sapExists) {
            this._onUI5Page = true
            this._initWdi5()
        } else {
            this._onUI5Page = false
        }
    }

    login() {
        // on login page
        this._doLogin()
        // do some wait
        const welcomeText = this.browser.$(
            this._xPath.mainSection + "/div[2]/div/div/div[1]/div/div/div/div/div/div/div[1]/div/div[1]"
        )
        this.browser.waitUntil(() => welcomeText.isExisting())

        this._checkForUi5PageAndInit()
    }

    _initWdi5() {
        // on ui5 page
        wdi5(this.browser)
        const config = wdi5().getUtils().getConfig()
        wdi5()
            .getLogger()
            .log("configurations: " + JSON.stringify(config))
    }

    naviagte(target) {
        if (this._onUI5Page) {
            wdi5().getUtils().goTo(target)
        }
    }

    waitFor() {
        if (!this._onUI5Page) {
            // logged in indicator of username and pw
            this.browser
                .$(this._xPath.mainSection + "/div[1]/div[1]/div/div[1]/div/div/ul[2]/li[2]/div/a/span")
                .waitForDisplayed()
        } else {
            console.warn("wait not possible on ui5 page")
        }
    }

    isUserLoggedIn() {
        if (this._onUI5Page) {
            const selector = {
                selector: {
                    bindingPath: {
                        modelName: "i18n",
                        propertyPath: "subtitle"
                    },
                    controlType: "sap.m.Title",
                    viewName: "com.daad.move.portal.pbfstartedapplications.view.Main"
                }
            }
            return this.browser.asControl(selector).hasStyleClass("subtitle-h3")
        } else {
            console.error("cannot check login status if the page does not contain sap and wdi5 namespace")
            return false
        }
    }

    isUserNotLoggedIn() {
        if (!this._onUI5Page) {
            // logout is not a ui5 page
            const welcomeText = this.browser.$("#welcomeText")
            return welcomeText.isExisting()
        } else {
            console.error("page contains ui5 namespcae -> not the login page")
            return false
        }
    }

    logout() {
        this.browser.$(this._xPath.userMenu + "/a/span").click()
        this.browser.$(this._xPath.userMenu + "/div/ul/li[3]/a").click()
    }
}

// singleton
module.exports = (browser) => {
    if (!_instance && browser) {
        _instance = new funding(browser)
    }
    return _instance
}
