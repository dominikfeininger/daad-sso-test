const page = require("./page")

let _instance = null

class myguide extends page {
    _url = "https://myguide.de"
    _title = "My GUIDE - The gateway to your German university"
    _xPath = {
        userMenu: "/html/body/div/div/div/div[2]/header/div[1]/div/div/div[2]/a/span/span",
        loginButton: "/html/body/div/div/div/div[2]/header/div/div/div/div[2]/a[1]/span",
        logoutMenu: "/html/body/div/div/div/div[2]/header/div[1]/div/div/div[2]/ul",
        mainHeader: "/html/body/div/div/div/main/div/div/div[1]/div[1]/div/h1"
    }
    _ids = {
        userMenu: "qa-user-menu",
        loginButton: "qa-login-button",
        loginButtonMobile: "qa-login-button-mobile", // unused mobile login button
        logoutMenu: "", // -> userMenuExpanded
        mainHeader: "qa-welcome-header",
        userMenuExpanded: "qa-user-menu-expanded", // unused
        menuLogout: "qa-logout-button"
    }

    constructor(browser) {
        super(browser)
        return this
    }

    isUserLoggedIn() {
        const usernameDisplay = this.browser.$(this._ids.userMenu)
        const result = usernameDisplay.getText()
        // TODO: change to generic expectaion
        return result === "TestVorname TestNachna"
    }

    isUserNotLoggedIn() {
        const loginButton = this.browser.$(this._ids.loginButton)
        this.browser.waitUntil(() => loginButton.isExisting())
        return loginButton.getText() === "Log in or register" // TODO: replace string check
    }

    login() {
        // login button
        this.browser.$(this._ids.loginButton).click()

        // press login button
        this._doLogin()

        // check the browser title to verify
        // wait for the pageload
        this.browser.$(this._ids.mainHeader).waitForDisplayed()
    }

    triggerSSOLogin() {
        this.browser.$(this._ids.loginButton).click()
    }

    logout() {
        // open user menu
        this.browser.$(this._ids.userMenu).click()

        const menu = this.browser.$(this._ids.userMenuExpanded)
        // this.browser.waitUntil(() => menu.isExisting())
        menu.waitForDisplayed()

        // logout button
        const logoutButton = this.browser.$(this._ids.menuLogout) // menuLogout
        logoutButton.click()
    }
}

// singleton
module.exports = (browser) => {
    if (!_instance && browser) {
        _instance = new myguide(browser)
    }
    return _instance
}
