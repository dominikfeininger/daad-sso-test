const page = require("./page")

let _instance = null

class portal extends page {
    _url = "https://portal.daad.de" //B2C_1A_signup_signin_DAP_saml
    _title = "DAAD-ID"
    _xPath = {}

    constructor(browser) {
        super(browser)
        return this
    }

    isUserLoggedIn() {
        const welcomeText = this.browser.$("#welcomeText")
        this.browser.waitUntil(() => welcomeText.isExisting())
        return welcomeText.isExisting()
    }

    triggerSSOLogin() {
        this._doLogin()
    }

    logout() {
        const mastheadEntry = this.browser.$("#mastheadEntry")
        this.browser.waitUntil(() => mastheadEntry.isExisting())

        const logoutButton = this.browser.$("#buttonlogoff")
        this.browser.waitUntil(() => logoutButton.isClickable())
        logoutButton.click()

        // confirmation dialog
        const confYesButton = this.browser.$("#button_std_yes")
        this.browser.waitUntil(() => confYesButton.isExisting())
        confYesButton.click()
    }

    awaitRedirect() {
        // username input
        const usernameInput = this.browser.$(this._xPathPage.loginForm + "/div[1]/input")
        usernameInput.waitForDisplayed()
    }

    login() {
        // enter text and press login button
        this._doLogin()
    }

    isUserNotLoggedIn() {
        const logoffForm = this.browser.$("#tblFrmUI")
        this.browser.waitUntil(() => logoffForm.isExisting())
        const logoffTable = this.browser.$("#tblInnerCnt")
        return logoffTable.isExisting()
    }
}

// singleton
module.exports = (browser) => {
    if (!_instance && browser) {
        _instance = new portal(browser)
    }
    return _instance
}
