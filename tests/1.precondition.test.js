const funding = require("../pageObjects/funding")
const mydaad = require("../pageObjects/mydaad")
const myguide = require("../pageObjects/myguide")
const portal = require("../pageObjects/portal")

describe("precodition: check availability of all SPs", () => {
    it("should open mydaad and check for (My|Mein) DAAD", () => {
        mydaad().open()
        expect(mydaad().browser.getTitle()).toMatch(/\w.* DAAD/)
    })

    it("should open portal and wait until redirected to IdP login page", () => {
        portal().open()
        portal().awaitRedirect()
        expect(portal().browser.getTitle()).toEqual(portal()._titleLoginPage)
    })

    it("should open myguide and check for 'My GUIDE - ...'", () => {
        myguide().open()
        expect(myguide().browser.getTitle()).toEqual(myguide().getTitle())
    })

    it("should open funding and wait until redirected to IdP login page", () => {
        funding().open()
        expect(funding().browser.getTitle()).toEqual(funding()._titleLoginPage)
    })
})
