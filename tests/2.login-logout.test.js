// import command access
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv

// import page objects
const fundingActions = require("../actions/funding.action")
const mydaadActions = require("../actions/mydaad.action")
const myguideActions = require("../actions/myguide.action")
const portalActions = require("../actions/portal.action")

// *** check arguments *** //
let sloHost = -1;
let sloType = "random"
if (argv.sloHost) {
    sloHost = argv.sloHost;
    sloType = "defined"
    console.log('slo host: ' + sloHost)
} else {
    console.error('slo host was not defined')
}

let ssoHost = -1;
let ssoType = "random"
if (argv.ssoHost) {
    ssoHost = argv.ssoHost;
    ssoType = "defined"
    console.log('sso host: ' + ssoHost)
} else {
    console.error('sso host was not defined')
}

const sp = [fundingActions, mydaadActions, myguideActions, portalActions]
// create random index
const randomIndexSSO = Math.floor(Math.random() * sp.length) + 1 - 1
const randomIndexSLO = Math.floor(Math.random() * sp.length) + 1 - 1
// *** for defined selection *** //
// needs to match the index at array variable "sp"
const listOfHosts = {
    mydaad: 1,
    funding: 0,
    myguide: 2,
    portal: 3
}

// *** use defined host or random *** //
// matches the number in array index of the selected host
const indexSLO = sloHost !== -1 ? listOfHosts[sloHost] : randomIndexSLO;
const indexSSO = ssoHost !== -1 ? listOfHosts[ssoHost] : randomIndexSSO;

// set the page object of the selected host
const initalSLO = sp[indexSLO];
const initalSSO = sp[indexSSO];

describe(`SSO on all SPs, starting w/ ${ssoType} SP: ${ssoHost}`, () => {
    initalSSO.doLogin()
    // check sso log in status at all SPs other than the initial one
    sp.forEach((someSP, index) => {
        if (index !== indexSSO) {
            someSP.testLogin()
        }
    })
})

describe(`SLO on all SPs, starting w/ ${sloType} SP: ${sloHost}`, () => {
    initalSLO.logout()
    // check slo log out status at all SPs other than the initial one
    sp.forEach((someSP, index) => {
        if (index !== indexSLO) {
            someSP.checkLogout()
        }
    })
})
