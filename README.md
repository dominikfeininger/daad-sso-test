# SSO- + SLO-Test for systems consituting the DAAD IdM

Suite of tests running on every service provide of the DAAD IdM: first, SSO is done; then, SLO is attempted.  
Test subject is the IdM reference user, tests are conducted in the IdM production landscape.  
For SSO, a random SP is picked to start the SSO journey from. Then the login status is validated in all other SPs.  
Same for SLO: a random SP is picked to start the SLO with. Subsequently, all other SPs are checked for a "log out"-status.

## Requirements

- Node.js `lts/erbium` (12.18.x)
- `.env` file containing the reference user's credentials

  ```
  USERNAME=<e-mail-address>
  PASSWORD=<password>
  ```

- List of hosts:
  - mydaad
  - myguide
  - funding
  - portal

## Run

0. `nvm use`
1. `npm i`
2. `npm start`
3. check result

### Run with host parameter

`npm start --ssoHost=<host> --sloHost=<host>`
if host is not set random will be selected

## Structure

- tests in `tests` folder
- tests contain actions or tests
- all sites' access are located in "pageObjects" folder
